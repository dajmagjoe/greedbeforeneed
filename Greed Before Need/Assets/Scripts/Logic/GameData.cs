﻿using System.Collections.Generic;
using Logic;

// DW: Namespace
public class GameData
{
    public List<Player> _players = new List<Player>();
    public List<Monster> _monsterDeck = new List<Monster>();   
    public Monster _currentMonster;                            

    public int CurrentPlayerTurn;       
    
    // DW: What's this used for?
    public int LastPlayerTurn;      
    
    // DW: IsMonsterTurn
    public bool monsterTurn;        //TODO: This is handled in GameState. Refactor.
    
    // DW: Move these to configs
    public float TimeAllowedToPlayActionCard = 10.0f;
    public float TimeAllowedToPlayReactionCard = 5.0f;
    public float TimeAllowedForMonsterTurn = 3.0f;
    public float countdownValue;

    public int currentHealth;                                   
}
