﻿using System.Collections.Generic;

namespace Logic
{
    public class Player
    {
        // DW: Naming conventions
        public string playerName;
        public int currentWealth;
        public int playerID;

        public List<Card> hand = new List<Card>();

    }
}