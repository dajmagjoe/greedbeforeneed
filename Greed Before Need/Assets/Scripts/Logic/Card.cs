﻿// DW: Namespace
public class Card
{
    public enum CardType
    {
        Action = 0,
        Reaction = 1,
        Monster = 2
    };

    // DW: Naming conventions -> Public is always capitalized
    public int cardID;
    public CardType cardType;
    public int wealthMod;
    public int healthMod;
    public int damageMod;
}