﻿using System;
using System.Collections.Generic;
using Infrastructure;
using Messages;
using UnityEngine;
using View;
using Random = System.Random;

//TODO: Unfuck the way Cards work. There cannot be "Card" and "oldCard" classes.
//TODO: Look for cases of doing int math on an enum. Could spell bad news later on.
//TODO: Add GameState to GameData

//Changes 6-26-20: Removed RunGame()

namespace Logic
{
    public class GameLogic
    {
        enum GameState                                              //GameData?
                                                                    //Break GameInProgress into distinct sub-states.
        {
            PlayersAreJoining = 0,
            GameSetup = 1,
            GameInProgress = 2,
            Results = 3,
        }

        enum TurnState
        {
            DrawingCards = 0,            
            SelectingAction = 1,
            PlayingAction = 2,
            ReactionResolve = 3,         //This will be skipped if no reaction
            ActionResolve = 4,
            MonsterAction = 5,
        }

        public GameData _GameData = new GameData();

        // DW: Let's move this into the game data class
        private GameState _gameState = GameState.PlayersAreJoining;   //Will we need to pass this on as GameData?
        private TurnState _turnState = TurnState.DrawingCards;

        // DW: Let's toss this stuff in a GameConfig class
        //Change this later to make it so 5 is the default hand size, but not max.
        public int maxHandSize = 5;                                   //Will we need to pass this on as GameData?
        public int startingWealth = 5;                                //Will we need to pass this on as GameData?

        public int startingHealth;
        public int healthPerPlayer = 10;

        // DW: Let's make a random number class to wrap all this ugliness
        private Random _randomNumber = new Random();
        
        // DW: -> Game Data
        private Card pendingCard;
        
        // DW: -> Config
        private float AICardSelectDelay;

        // DW: -> Game Data
        private int cardIDindex = 0;

        public void Tick(float deltaTime)
        {
            if(_gameState != GameState.GameInProgress)
            {
                return;
            }

            switch (_turnState)
            {
                case TurnState.DrawingCards:
                {
                        //Call DrawCard for the current player a minimum of one time. Ensure player has at least 5 cards in hand.
                        //Proceed to SelectingAction state.
                    break;
                }
                case TurnState.SelectingAction:
                {
                        _GameData.countdownValue -= deltaTime;
                        MessageRouter.Instance.RaiseMessage(new TimerUpdateMessage { TimerValue = _GameData.countdownValue });


                        if (_GameData.countdownValue <= 0f || _GameData.CurrentPlayerTurn != 0) //This a bullshit human check. Design a real one. THIS WILL NOT WORK IF MULTIPLE HUMANS ARE INVOLVED
                        {
                            if(_GameData.countdownValue <= (_GameData.TimeAllowedToPlayActionCard - AICardSelectDelay)) // This check makes it so the computer takes AICardSelectDelay seconds before choosing their first action card.
                            {
                                //pendingCard = _GameData._players[_GameData.CurrentPlayerTurn].hand[GetFirstActionCardFromHand(_GameData._players[_GameData.CurrentPlayerTurn])];
                                PlayFirstActionCard(_GameData._players[_GameData.CurrentPlayerTurn]);
                                _turnState = TurnState.PlayingAction;
                                break;
                            }

                            //Player didn't play a card in time.
                            //If the player doesn't select a card before the timer expires, play the first available action card.
                            if (_GameData.CurrentPlayerTurn == 0) //This a bullshit human check. Design a real one. THIS WILL NOT WORK IF MULTIPLE HUMANS ARE INVOLVED)
                            {
                                //pendingCard = _GameData._players[_GameData.CurrentPlayerTurn].hand[GetFirstActionCardFromHand(_GameData._players[_GameData.CurrentPlayerTurn])];
                                PlayFirstActionCard(_GameData._players[_GameData.CurrentPlayerTurn]);
                                _turnState = TurnState.PlayingAction;
                            }
                        }
                        //Players have a set amount of time to select a card.
                        break;
                }
                case TurnState.PlayingAction:
                {
                        //Somewhere after the player has selected an action, set _GameData.countdownValue to a new timer value.
                        //This is a set amount of time in which the stats on the played card are revealed to others
                        //In this time, players can play reaction cards.

                        _GameData.countdownValue -= deltaTime;
                        MessageRouter.Instance.RaiseMessage(new TimerUpdateMessage { TimerValue = _GameData.countdownValue });

                        MessageRouter.Instance.RaiseMessage(new UIGameStatusMessage
                        {
                            UIGameStatus = "Player " + (_GameData.CurrentPlayerTurn + 1) + " has played:\n" +
                            "CARD<H: " + pendingCard.healthMod + ", W: " + pendingCard.wealthMod + ", D: " + pendingCard.damageMod + ">"
                        }); ;

                        if (_GameData.countdownValue <= 0f)
                        {
                            //No player played a reaction. Move directly to TurnState.ActionResolve.
                            _turnState = TurnState.ActionResolve;
                        }
                        break;
                }
                case TurnState.ReactionResolve:
                {
                        //If a player plays an action card, this is where it's announced and takes affect.
                    break;
                }
                case TurnState.ActionResolve:
                {
                        //The selected action card resolves
                        if(pendingCard != null)
                        {
                            PlayCard(_GameData._players[_GameData.CurrentPlayerTurn], pendingCard);
                        }
                        break;
                }
                case TurnState.MonsterAction:
                {
                        //The monster selects its action from a list
                        _GameData.countdownValue -= deltaTime;
                        MessageRouter.Instance.RaiseMessage(new TimerUpdateMessage { TimerValue = _GameData.countdownValue });

                        if (_GameData.countdownValue <= 0f)
                        {
                            MonsterTurnMessage message = new MonsterTurnMessage { monster = _GameData._currentMonster, };
                            MessageRouter.Instance.RaiseMessage(message);
                            _GameData.countdownValue = 100f; // TEST VALUE -- Figure out how to set this more appropriately.
                        }

                        //The action is processed
                        //The players cry
                        //Move to Draw Card
                        break;
                }
            }


        }

        public void JoinLobby(string name)
        {
            if (_gameState != GameState.PlayersAreJoining)
            {
                return;
            }
            
            // DW: Let's move this magic number 6 into a config class
            if(_GameData._players.Count >= 6)
            {
                Debug.Log("There are too many players in the game. Stop trying to join in on other peoples' fun");
                return;
            }
        
            // DW: We can simplify a bunch here and reduce nesting.
            if(_GameData._players.Count != 0)
            {
                foreach(Player player in _GameData._players)
                {
                    if(player.playerName == name)
                    {
                        Debug.LogError("A player by that name is already in the game");
                        return;
                    }
                }
            }

            Player newPlayer = new Player
            {
                playerName = name
            };

            MessageRouter.Instance.RaiseMessage(new PlayerJoinedMessage
            {
                PlayerId = _GameData._players.Count,
                PlayerName = name
            });

            // DW: Let's do all the logic first and then broadcast the message after. _players.Count is accessed twice.
            newPlayer.playerID = _GameData._players.Count;
            _GameData._players.Add(newPlayer);
        }

        public void BeginGame()
        {
            // DW: This requirement to start will grow. Let's create a CanStartGame() function.
            
            // Handle 0 players or min players (create variable?)
            _gameState = GameState.GameSetup;
            SetupGame();
        }

        public void SetupGame()
        {
            startingHealth = healthPerPlayer * _GameData._players.Count;
            _GameData.currentHealth = startingHealth;
            //Adds 0 health to the party but updates the view (Health is set properly above)
            ModifyPartyHealth(0);
            Debug.Log("We're setting up the game! There are " + _GameData._players.Count + " players with a total of " + _GameData.currentHealth + " health.");

            foreach (var player in _GameData._players)
            {
                ModifyPlayerGold(player, startingWealth);
                FillHand(player);
            }

            CreateMonsterDeck();

            Debug.Log("The game is set! The players will face off against " + _GameData._monsterDeck.Count + " monsters.");

            _GameData._currentMonster = _GameData._monsterDeck[0];

            // DW: Let's move this to the end. Or perhaps we can create a function as this message is sent out in 3 different functions.
            MessageRouter.Instance.RaiseMessage(new MonsterUpdateMessage
            {
                Monster = _GameData._currentMonster,
                MonsterHealthDelta = _GameData._currentMonster.currentHealth,
                MonsterHealthTotal = _GameData._currentMonster.currentHealth,
                MonsterWealthDelta = _GameData._currentMonster.currentWealth,
                MonsterWealthTotal = _GameData._currentMonster.currentWealth
            });
            //_GameData._players.Shuffle();

            _gameState = GameState.GameInProgress;
        }

        // DW: Let's make this private. We should never be modifying it from outside the logic class.
        public void ModifyPartyHealth(int healthDelta)
        {
            _GameData.currentHealth += healthDelta;

            MessageRouter.Instance.RaiseMessage(new PlayerHealthUpdateMessage
            {
                HealthDelta = healthDelta,
                TotalHealth = _GameData.currentHealth,
            });
        }

        // DW: Same here. Let's make it private.
        public void ModifyPlayerGold(Player player, int goldDelta)
        {
            player.currentWealth += goldDelta;

            MessageRouter.Instance.RaiseMessage(new GoldUpdateMessage
            {
                PlayerId = player.playerID,
                GoldDelta = goldDelta,
                TotalGold = player.currentWealth,
            });
 
        }

        // DW: Let's do this now
        // TODO: Change to return the index of the player
        public void DetermineWealthLeader()
        {
            //This doesn't account for ties yet.

            int winnerIndex = 0;

            // DW: Can be foreach
            for(int i = 0; i < _GameData._players.Count; i++)
            {
                if(_GameData._players[i].currentWealth > _GameData._players[winnerIndex].currentWealth)
                {
                    winnerIndex = i;
                }
            }

            if(_GameData.currentHealth > 0)
            {
                Debug.LogError("The winner is: " + _GameData._players[winnerIndex].playerName + " with " + _GameData._players[winnerIndex].currentWealth + " gold!");
            }
            else
            {
                Debug.LogError("PLAYERS LOSE");
            }
        }

        //This function might be worthless. Commenting out until we can delete it later.
        //private void DoPlayerTurn(Player player)
        //{
        //    FillHand(player);
        
        //    PlayFirstActionCard(player);

        //}

        public void PlayAICard(int playerID)
        {
            //Add range check, add human check. Make sure the computer isn't playing a card for the player
            if(playerID != _GameData.CurrentPlayerTurn)
            {
                return;
            }

            PlayFirstActionCard(_GameData._players[playerID]);
        }
        private void PlayFirstActionCard(Player player)
        {
            // Find the index of the first action card.
            int actionCardIndex = GetFirstActionCardFromHand(player);
            if(actionCardIndex == -1)
            {
                Debug.LogError("You done goofed! No actions in hand");
                return;
            }

            pendingCard = player.hand[actionCardIndex];
            MessageRouter.Instance.RaiseMessage(new SelectCardMessage { CardID = pendingCard.cardID, playerID = player.playerID });
            _GameData.countdownValue = _GameData.TimeAllowedToPlayReactionCard;

            //PlayCard(player, player.hand[actionCardIndex]);
        }

        private int GetFirstActionCardFromHand(Player player)
        {
            for (int i = 0; i < player.hand.Count; i++)
            {
                if (player.hand[i].cardType == Card.CardType.Action)
                {
                    return i;
                }
            }

            return -1;
        }

        public void MonsterTurn(Monster currentMonster)
        {
            ModifyPartyHealth(-_GameData._currentMonster.damage);
            Debug.Log(currentMonster.name + " just did " + currentMonster.damage + " damage to the party!. The party has " + _GameData.currentHealth + " health left.");
            _turnState = TurnState.DrawingCards;
            AdvanceGameState();
        }

        // DW: Let's rewrite this badboy. I thought it was clever, but it's difficult to read.
        private void FillHand(Player player)
        {
            while(player.hand.Count < maxHandSize)
            {
                Card newCard = DrawCard();
                newCard.cardID = cardIDindex;
                cardIDindex++;
                player.hand.Add(newCard);

                MessageRouter.Instance.RaiseMessage(new CreateCardMessage
                {
                    WealthMod = newCard.wealthMod,
                    HealthMod = newCard.healthMod,
                    DamageMod = newCard.damageMod,
                    PlayerId = player.playerID,
                    CardID = newCard.cardID,
                    Player = player
                });
            }

            AICardSelectDelay = UnityEngine.Random.Range(2.0f, 5.0f); // There's no real reason for this to be here other than I know it needs to happen before we hit TurnState.SelectingAction

            // Advance the turn state, start the timer, and return if action card is found.
            for (int i = 0; i < player.hand.Count; i++)
            {
                if(player.hand[i].cardType == Card.CardType.Action)
                {
                    _GameData.countdownValue = _GameData.TimeAllowedToPlayActionCard; // The player has _GameData.TimeAllowedToPlayActionCard seconds to select a card
                    _turnState = TurnState.SelectingAction;
                    MessageRouter.Instance.RaiseMessage(new UIGameStatusMessage
                    {
                        UIGameStatus = "Player " + (_GameData.CurrentPlayerTurn + 1) + 
                        "\n is selecting a card."
                    });
                    return;
                }
            }

            // Add handling for infinite hand clear/fill
            player.hand.Clear();
            FillHand(player);
        }

        public void PlayPlayerCard(int playerID, int cardID)
        {
            //Search through the hand of a certain player for card with matching cardID
            //Check to make sure that the card belongs to the player who called this
            //Send the Player and Card class to PlayCard()
            //Tell the view to destroy the card

            Player player = null;
            Card card = null;

            if(_GameData.CurrentPlayerTurn != playerID || _GameData.monsterTurn)
            {
                return;
            }

            foreach(Player p in _GameData._players)
            {
                if(playerID == p.playerID)
                {
                    player = p;
                }
            }

            if (player == null)
            {
                Debug.LogError("Invalid player");
                return;
            }

            foreach (Card c in player.hand)
            {
                if(cardID == c.cardID)
                {
                    card = c;
                }
            }

            if(card == null)
            {
                Debug.LogError("Invalid card");
                return;
            }

            if (card.cardType == Card.CardType.Action && _turnState == TurnState.SelectingAction)
            {
                //The player has played an action. Change state to PlayingAction and start reaction timer.
                _GameData.countdownValue = _GameData.TimeAllowedToPlayReactionCard;
                pendingCard = card;
                MessageRouter.Instance.RaiseMessage(new SelectCardMessage { CardID = cardID, playerID = playerID });
                _turnState = TurnState.PlayingAction;
                MessageRouter.Instance.RaiseMessage(new UIGameStatusMessage
                {
                    UIGameStatus = "Player " + (_GameData.CurrentPlayerTurn + 1) + " has played:\n" + 
                    "CARD<H: " + card.healthMod + ", W: " + card.wealthMod + ", D: " + card.damageMod + ">" 
                });;
            }
        }

        private void PlayCard(Player player, Card card)
        {

            Debug.Log(player.playerName + " plays a card. H: " + card.healthMod + " W: " + card.wealthMod + " D: " + card.damageMod);
            ModifyPartyHealth(card.healthMod);
            ModifyPlayerGold(player, card.wealthMod);
            ModifyMonsterHealth(-card.damageMod);

            pendingCard = null;

            MessageRouter.Instance.RaiseMessage(new DestroyCardMessage { CardID = card.cardID,
                playerID = player.playerID});
            player.hand.Remove(card);

            Debug.Log("Current monster has " + _GameData._currentMonster.currentHealth + " health remaining.");
            Debug.Log("The player's wealth is modified by " + card.wealthMod + ". " + player.playerName + " now has " + player.currentWealth + " gold!");

            AdvanceGameState();
        }

        private void ModifyMonsterHealth(int damageMod)
        {
            _GameData._currentMonster.currentHealth += damageMod;
            MessageRouter.Instance.RaiseMessage(new MonsterUpdateMessage { Monster = _GameData._currentMonster,
                MonsterHealthDelta = damageMod,
                MonsterHealthTotal = _GameData._currentMonster.currentHealth,});

        }

        private void AdvanceGameState()
        {
            CheckForMonsterDeath();
            CheckForPlayerDeath();

            if(_gameState == GameState.Results)
            {
                DetermineWealthLeader();
                return;
            }

            DetermineNextPlayer();
        }

        private void CheckForPlayerDeath()
        {
            if(_GameData.currentHealth <= 0)
            {
                _gameState = GameState.Results;
            }
        }

        private void DetermineNextPlayer()
        {
            //Determine who just went
            //Increment the playerID
            //If the incremented playerID is equal/greater than 3, substract 3.
            //Check to see if MonsterTurn is true or false
            //If false, make Monster Turn true and have the monster take a turn.
            //If true, turn Monster Turn false and it's playerID's turn

            if (_GameData.monsterTurn == false)
            {
                _GameData.monsterTurn = true;
                Debug.Log("It is now monster: " + _GameData._currentMonster.name + "'s turn.");
                _turnState = TurnState.MonsterAction;
                _GameData.countdownValue = _GameData.TimeAllowedForMonsterTurn;

                MessageRouter.Instance.RaiseMessage(new UIGameStatusMessage
                {
                    UIGameStatus = _GameData._currentMonster.name + " is about to do \n" +
                    _GameData._currentMonster.damage + " damage to the party."
                });
                //Check to see if we have a monster to avoid throwing a null reference
                //MonsterTurn(_currentMonster);
                return;
            }

            _GameData.LastPlayerTurn = _GameData.CurrentPlayerTurn;
            _GameData.CurrentPlayerTurn++;

            if(_GameData.CurrentPlayerTurn >= _GameData._players.Count)
            {
                _GameData.CurrentPlayerTurn = 0;
            }

            Debug.Log("Moving on to Player " + _GameData.CurrentPlayerTurn + "'s turn.");
            FillHand(_GameData._players[_GameData.CurrentPlayerTurn]);

            _GameData.monsterTurn = false;
            
            MessageRouter.Instance.RaiseMessage(new PlayerTurnMessage { playerID = _GameData.CurrentPlayerTurn });
        }

        private void CheckForMonsterDeath()
        {
            if (_GameData._currentMonster.currentHealth > 0)
            {
                return;
            }

            KillMonster(_GameData._players[_GameData.CurrentPlayerTurn]);

            if (_GameData._monsterDeck.Count > 0)
            {
                Debug.Log("Drawing new monster.");
                DrawNewMonster();
            }
            else
            {
                Debug.Log("All monsters have been defeated.");
                _gameState = GameState.Results;
            }

        }

        private void KillMonster(Player player)
        {
            ModifyPlayerGold(player, _GameData._currentMonster.currentWealth);
            _GameData._monsterDeck.Remove(_GameData._currentMonster);
            Debug.Log("+++ " + _GameData._currentMonster.name + " was defeated.");
        }

        private Card DrawCard()
        {
            Card newCard = new Card {

                //For right now, all cards are Action cards.
                cardType = Card.CardType.Action,
                //cardType = (Card.CardType)GetRandomNumberInRange (0, 1),
                wealthMod = GetRandomNumberInRange (-5, 5),
                healthMod = GetRandomNumberInRange (-3, 5),
                damageMod = GetRandomNumberInRange (1, 3),
            };

            return newCard;
        }
    
        public void CreateMonsterDeck()
        {
            // This creates a deck of X+1 monsters, where X is the number of players.
            for(int i = 0; i < (_GameData._players.Count + 1); i++)
            {
                _GameData._monsterDeck.Add(new Monster
                    {
                        name = "Monster " + i,
                        currentHealth = GetRandomNumberInRange(3, 5),
                        currentWealth = GetRandomNumberInRange(5, 10),
                        damage = GetRandomNumberInRange(3, 5)
                    }
                ) ;
            }
        }

        private int GetRandomNumberInRange(int min, int max)
        {
            return _randomNumber.Next(min, max + 1);
        }

        private void DrawNewMonster()
        {
            //I think this is destroying the first monster right now. Add handling for this.

            _GameData._currentMonster = _GameData._monsterDeck[0];

            MessageRouter.Instance.RaiseMessage(new MonsterUpdateMessage
            {
                Monster = _GameData._currentMonster,
                MonsterHealthDelta = _GameData._currentMonster.currentHealth,
                MonsterHealthTotal = _GameData._currentMonster.currentHealth,
                MonsterWealthDelta = _GameData._currentMonster.currentWealth,
                MonsterWealthTotal = _GameData._currentMonster.currentWealth
            });
            Debug.Log("A new monster has been drawn. There are " + _GameData._monsterDeck.Count + " monsters left.");

        }
    }

}

//Players join the lobby.
//When all players are ready, the host starts the game.
//All players draw 5 cards, the appropriate number of monster cards are drawn and laid flat, and the order of the players is randomized.
//The “first” player is set to the beginning of the player queue.
//The “first” monster is set to the first monster in the monster queue.
//It is the next player in the player queue’s turn.
//If the monster was slain by the previous player or this is the first turn, the next monster card in the monster queue is turned over, revealing the foe.
//If the current player has fewer than 5 cards, the player draws up to 5 cards.
//If a player has all reaction cards and no action cards, that player continues drawing until they draw an action card. The player then discards cards of their choosing until they have a total of 5 cards in their hand. 
//The current player has 15 seconds to select an action card. The player then plays the selected card.
//Other players are shown this card for 3 seconds.
//If another player plays a reaction card, that card will be executed immediately after the action against which it was played.
//If no player plays a reaction card within these 3 seconds, the action card is executed.
//If this action card reduces the party’s health to 0, the game is over and the players lose.
//If a reaction card is played on an action and reduces the party’s health to zero, the players lose.
//If the monster’s health is reduced to zero, it dies (unless the card states otherwise).
//The player who dealt the damage that reduced the monster’s health to zero receives any bounty the monster had.
//If another player plays a reaction card, but the monster dies from the action, the reaction card does no damage. The other effects of the reaction card still take place.
//If this is the last monster, the game is over and a winner is determined (SEE STEP #12).
//If this is not the last monster, return to step 6.
//If the monster is not dead, the monster acts.Roll a d6 to determine which action the monster takes.
//If the monster’s action reduces the party’s health to 0, the game is over and the players lose.
//Otherwise, return to step 6.
//When all monsters have been slain, all players’ wealth total is revealed.The player with the most wealth wins.
//If two or more players have the same amount of gold at the game’s end, those players are entered into a lightning round. Another creature card is drawn.
//Only those who were tied for the most gold continue on to this round.
//Return to step 6.