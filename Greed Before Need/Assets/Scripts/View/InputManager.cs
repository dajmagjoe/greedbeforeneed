﻿using UnityEngine;

namespace View
{
    // DW: PlayerInputManager?
    public class InputManager : MonoBehaviour
    {
        
        public GameObject MouseTarget = null;
        public float MaxTargetDistance = 5.0f;
        public GameManager GameManager;
        
        // DW: Let's be more descriptive about this
        int layerMask;

        // Start is called before the first frame update
        void Start()
        {
            // DW: Let's go ahead and cache the game logic class instead as that's the only thing we will ever use.
            GameManager = FindObjectOfType<GameManager>();
            layerMask = (1 << 8);
        }

        // Update is called once per frame
        void Update()
        {
            // DW: Cache Camera.main. It's an "expensive" lookup each time
            if(Physics.Raycast((Camera.main.transform.position), Camera.main.transform.forward, out RaycastHit hitinfo, MaxTargetDistance, layerMask))
            {

                //Debug.Log("We hit card with ID: " + hitinfo.collider.gameObject.GetComponent<OldCard>().CardID);
                MouseTarget = hitinfo.transform.gameObject;
            }
            else
            {
                MouseTarget = null;
            }

            if(Input.GetMouseButtonDown(0) && MouseTarget != null)
            {
                // DW: Get component is potentially expensive. You're also grabbing it here, twice.
                GameManager.GetGameLogic().PlayPlayerCard(MouseTarget.GetComponent<ViewCard>().OwnerID, MouseTarget.GetComponent<ViewCard>().CardID);
            }
        }
    }
}
