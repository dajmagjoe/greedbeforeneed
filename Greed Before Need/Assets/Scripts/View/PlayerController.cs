﻿using System;
using System.Collections;
using System.Collections.Generic;
using Infrastructure;
using Messages;
using UnityEngine;

namespace View
{
    // DW: Let's organize all of these classes with a sentence or two about what the class should do.
    // DW: This class is a chunker
    public class PlayerController : MonoBehaviour
    {
        public bool IsPlayerAHuman;
        public Camera MainCamera;
        public InputManager InputManager;
        public MouseLook MouseLook;
        public GameObject cardPrefab;
        public List<GameObject> HandCards = new List<GameObject>();
        public Transform Hand;
        public Transform HandBoundMin;
        public Transform HandBoundMax;
        public float MaxCardInHandAngle = 15f;
        public float CardInHandDipCoefficient = 1f;
        public GameManager GameManager;

        private int _playerID;
    
        private void Awake()
        {
            MessageRouter.Instance.AddHandler<CreateCardMessage>(CreateCard);
            MessageRouter.Instance.AddHandler<DestroyCardMessage>(DestroyCard);
            MessageRouter.Instance.AddHandler<SelectCardMessage>(HighlightCard);
            MessageRouter.Instance.AddHandler<PlayerTurnMessage>(OnPlayerTurn);
        }

        private void HighlightCard(SelectCardMessage message)
        {
            for (int i = HandCards.Count - 1; i >= 0; i--)
            {
                GameObject go = HandCards[i];
                if (go.GetComponent<ViewCard>().CardID == message.CardID)
                {
                    go.GetComponent<MeshRenderer>().material.color = Color.yellow;
                }
            }

        }

        private void Start()
        {
            GameManager = FindObjectOfType<GameManager>();
        }

        private void OnPlayerTurn(PlayerTurnMessage message)
        {
            Debug.Log("Starting Player Turn Coroutine");
            StartCoroutine(DoPlayerTurn(message));
        }

        private IEnumerator DoPlayerTurn(PlayerTurnMessage message)
        {
            Debug.Log(_playerID + " is attempting to start AI Turn for: " + message.playerID);
            yield return null;

            if (IsPlayerAHuman)
            {
                yield break;
            }

            if (message.playerID != _playerID)
            {
                yield break;
            }

            //We commented out the line below because the AI player logic is now being handled in GameLogic. Good idea? Not so sure.
            //GameManager.GetGameLogic().PlayAICard(_playerID);

        }

        // DW: Let's keep this function bunched closely with the Awake, where we do our registrations so we can see if we missed any
        private void OnDestroy()
        {
            MessageRouter.Instance.RemoveHandler<CreateCardMessage>(CreateCard);
            MessageRouter.Instance.RemoveHandler<SelectCardMessage>(HighlightCard);

        }

        // DW: All of these comments are very confusing to look at. If we ever need to go back, it's in source control.
        private void CreateCard(CreateCardMessage message)
        {
            if (!IsPlayerAHuman)
            {

            }
            if(message.PlayerId == _playerID)
            {
                //Vector3 modifiedPosition = Hand.position - (Hand.right * 0.3f * 2f);

                //modifiedPosition += Hand.right * (0.3f * HandCards.Count);

                GameObject newCard = GameObject.Instantiate(cardPrefab, Hand);
                newCard.GetComponent<ViewCard>().SetCardUIValues(message.WealthMod, message.HealthMod, message.DamageMod, message.CardID, message.PlayerId);
                HandCards.Add(newCard);

                //newCard.transform.localRotation = Quaternion.Euler(-70f, 0f, 0f);
                UpdateCardPositions();
                //newCard.transform.position = modifiedPosition;
                //newCard.transform.LookAt(transform, Vector3.forward);
            }
        }

        private void UpdateCardPositions()
        {
            //Determine how many cards we have in our hand
            //Determine the increment value by dividing the distance between HandBoundMin and HandBoundMax by HandCards.Count + 1
            //Iterate through the Cards in our Hand and...
            //...Use MoveCard to place each card at (HandBoundMin + IncrementValue * HandCard Index Value)
            int numCardsInHand = HandCards.Count;
            float cardBufferIncrement = 1f / (numCardsInHand + 1f);

            for (int i = 0; i < numCardsInHand; i++)
            {
                PositionCard(i, cardBufferIncrement * (i + 1), numCardsInHand);
            }
        }

        // DW: Can we normalize the comment format as // with a space in between?
        private void PositionCard(int cardIndex, float normalizedPosition, int numCardsInHand)
        {
            //Determines how far along the path from minBound to maxBound the card is
            float percentFromMinBound = (cardIndex + 1f) / (numCardsInHand + 1f);
            HandCards[cardIndex].transform.position = HandBoundMin.position + (normalizedPosition * (HandBoundMax.position - HandBoundMin.position));

            //Calculate y shift by using a parabolic formula
            float yShift = -Mathf.Abs( Mathf.Pow(((percentFromMinBound - 0.5f) * CardInHandDipCoefficient), 2f));
            HandCards[cardIndex].transform.position += new Vector3(0f, yShift,0f);

            HandCards[cardIndex].transform.localRotation = Quaternion.Euler(-70f, 0f, 0f);

            //Uses percentFromMinBound to determine if this card is left/right of center.
            if (percentFromMinBound < .5f)
            {
                float rot = ((cardIndex + 1f) / (numCardsInHand + 1f) - 1f) * MaxCardInHandAngle;

                HandCards[cardIndex].transform.Rotate(0f, rot, 0f);
            }

            if(percentFromMinBound > .5f)
            {
                float rot = ((cardIndex + 1f) / (numCardsInHand + 1f)) * MaxCardInHandAngle;
                HandCards[cardIndex].transform.Rotate(0f, rot, 0f);
            }
            //Position the Card at (HandBoundMin + ((IncrementValue * (Index of Card in Hand + 1)  * (HandBoundMax - HandBoundMin))
        }

        private void DestroyCard(DestroyCardMessage message)
        {
            for (int i = HandCards.Count - 1; i >= 0; i--)
            {
                GameObject go = HandCards[i];
                if (go.GetComponent<ViewCard>().CardID == message.CardID)
                {
                    HandCards.Remove(go);
                    Destroy(go);
                }
            }

        }


        public void SetPlayerData(bool isHuman, int playerID)
        {
            if (isHuman)
            {
                IsPlayerAHuman = true;
                MainCamera.enabled = true;
                InputManager.enabled = true;
                MouseLook.enabled = true;
                MainCamera.GetComponent<AudioListener>().enabled = true;
            }

            _playerID = playerID;
        }
    }
}
