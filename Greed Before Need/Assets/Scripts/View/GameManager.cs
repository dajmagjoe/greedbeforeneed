﻿using Logic;
using UnityEngine;

namespace View
{
    // DW: Comment about what the game manager does
    public class GameManager : MonoBehaviour
    {
        // DW: Naming for all 3 of these variables
        public int numPlayers = 3;
    
        public GameObject UIManager;
        
        // DW: It doesn't make sense that this is public if we have a function to get it below
        public GameLogic _GameLogic;

        private void Awake()
        {
            _GameLogic = new GameLogic();
        }
        private void Start()
        {

            for (int i = 0; i < numPlayers; i++)
            {
                _GameLogic.JoinLobby("Player " + i);
            }

            _GameLogic.BeginGame();

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        public GameLogic GetGameLogic()
        {
            return _GameLogic;
        }

        private void Update()
        {
            _GameLogic.Tick(Time.deltaTime);
        }
    }
}
