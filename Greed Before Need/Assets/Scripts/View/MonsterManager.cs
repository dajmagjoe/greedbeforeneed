﻿using System.Collections;
using Infrastructure;
using Logic;
using Messages;
using UnityEngine;

namespace View
{
    public class MonsterManager : MonoBehaviour
    {
        // DW: Fix
        GameManager GameManager;

        private void Awake()
        {
            MessageRouter.Instance.AddHandler<MonsterTurnMessage>(OnMonsterTurn);
        }

        private void Start()
        {
            GameManager = FindObjectOfType<GameManager>();
        }

        private void OnMonsterTurn(MonsterTurnMessage message)
        {
            Debug.Log("Starting monster coroutine");
            StartCoroutine(DoMonsterTurn(message));
        }

        private IEnumerator DoMonsterTurn(MonsterTurnMessage message)
        {
            yield return null;
            GameManager.GetGameLogic().MonsterTurn(message.monster);
        }
    }
}
