﻿using System;
using System.Collections.Generic;
using Infrastructure;
using Messages;
using TMPro;
using UnityEngine;

namespace View
{
    // DW: Let's rename this UiManager
    public class UIManager : MonoBehaviour
    {
        public List<TextMeshProUGUI> PlayerGoldLabels = new List<TextMeshProUGUI>();
        public TextMeshProUGUI PlayerHealthLabel;
        public TextMeshProUGUI CurrentMonsterName;
        public TextMeshProUGUI CurrentMonsterHealth;
        public TextMeshProUGUI CurrentMonsterWealth;
        public TextMeshProUGUI CurrentTimer;
        public TextMeshProUGUI UIGameStatus;

        private void Awake()
        {
            MessageRouter.Instance.AddHandler<GoldUpdateMessage>(UpdatePlayerGold);
            MessageRouter.Instance.AddHandler<MonsterUpdateMessage>(UpdateMonster);
            MessageRouter.Instance.AddHandler<TimerUpdateMessage>(UpdateTimer);
            MessageRouter.Instance.AddHandler<PlayerHealthUpdateMessage>(UpdatePlayerHealth);
            MessageRouter.Instance.AddHandler<UIGameStatusMessage>(UpdateUIGameStatus);

        }

        private void UpdateUIGameStatus(UIGameStatusMessage message)
        {
            UIGameStatus.text = message.UIGameStatus;
        }

        void UpdateTimer(TimerUpdateMessage message)
        {
            if(message.TimerValue <= 0)
            {
                CurrentTimer.text = "";
                return;
            }

            // DW: You can also use .ToString("0.00") to specify the rounding
            CurrentTimer.text = Math.Round(message.TimerValue, 1).ToString();
        }

        void UpdatePlayerHealth(PlayerHealthUpdateMessage message)
        {
            PlayerHealthLabel.text = message.TotalHealth.ToString();
        }

        void UpdatePlayerGold(GoldUpdateMessage message)
        {
            //TODO: Range Check -- Make sure the playerID range is logical
            PlayerGoldLabels[message.PlayerId].text = "Player " + (message.PlayerId + 1) + ": " + message.TotalGold;
        }

        void UpdateMonster(MonsterUpdateMessage message)
        {
            if(message.MonsterHealthDelta != 0)
            {
                CurrentMonsterHealth.text = message.MonsterHealthTotal.ToString();
            }

            if(message.MonsterWealthDelta != 0)
            {
                CurrentMonsterWealth.text = message.MonsterWealthTotal.ToString();
            }

            if(message.Monster.name != CurrentMonsterName.text)
            {
                CurrentMonsterName.text = message.Monster.name;
            }
        }
    }
}
