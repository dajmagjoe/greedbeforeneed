﻿using System.Collections.Generic;
using Infrastructure;
using Messages;
using UnityEngine;

namespace View
{
    public class PlayerManager : MonoBehaviour
    {

        public GameObject PlayerPrefab;
        public List<Transform> PlayerStartLocations = new List<Transform>();

        private void Awake()
        {

            MessageRouter.Instance.AddHandler<PlayerJoinedMessage>(OnPlayerJoined);
        }


        private void OnPlayerJoined(PlayerJoinedMessage message)
        {
            GameObject newPlayer = GameObject.Instantiate(PlayerPrefab, PlayerStartLocations[message.PlayerId]);

            // DW: Let's simplify this by using the player id as the boolean. 
            if(message.PlayerId == 0)
            {
                newPlayer.GetComponent<PlayerController>().SetPlayerData(true, message.PlayerId);
            }
            else
            {
                newPlayer.GetComponent<PlayerController>().SetPlayerData(false, message.PlayerId);
            }
            //Previously, this was what we used to determine if a player was a human. It would default to player 1 (ID = 0) being human.
            //newPlayer.GetComponent<PlayerLogic>().SetPlayerData(message.PlayerId == 0, message.PlayerId);
        }


    }
}
