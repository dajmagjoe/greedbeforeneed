﻿using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class ViewCard : MonoBehaviour
    {
        public bool isSelected = false;
        public Canvas cardUI;
        public int CardID;
        public int OwnerID;

        // Start is called before the first frame update
        void Awake()
        {
            // DW: We are exposing it above as public and then getting it here. Why not just assign?
            cardUI = GetComponentInChildren<Canvas>();
        }

        public void SetCardUIValues(int w, int h, int d, int cardID, int playerID)
        {
            // DW: Let's get hard inspector links to these.
            cardUI.transform.Find("HealthMod").GetComponent<Text>().text = h.ToString();
            cardUI.transform.Find("WealthMod").GetComponent<Text>().text = w.ToString();
            cardUI.transform.Find("DamageMod").GetComponent<Text>().text = d.ToString();
            CardID = cardID;
            OwnerID = playerID;
        }

        public void ToggleSelect()
        {
            //This is where we toggle select based on whether a card has been clicked on AND if it's the player's turn.
            //If it not a player's turn, the only card they can have selected is of type REACTION.
        }
    }
}
