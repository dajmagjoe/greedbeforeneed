﻿using Logic;

namespace Messages
{
    public class CreateCardMessage
    {
        public string CardName;
        public int WealthMod;
        public int HealthMod;
        public int DamageMod;
        // DW: Let's be consistent. The best way is Id
        public int PlayerId;
        public int CardID;
        // DW: Do we need to send the player?
        public Player Player;
    }
}
