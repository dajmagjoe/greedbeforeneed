﻿namespace Messages
{
    public class PlayerJoinedMessage
    {
        public int PlayerId;
        public string PlayerName;
        public int StartingMoney;
    }
}
