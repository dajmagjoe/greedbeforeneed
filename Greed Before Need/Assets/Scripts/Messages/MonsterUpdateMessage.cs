﻿using Logic;

namespace Messages
{
    public class MonsterUpdateMessage
    {
        // DW: Do we really need to send the monster?
        public Monster Monster;
        public int MonsterHealthDelta;
        public int MonsterHealthTotal;
        public int MonsterWealthDelta;
        public int MonsterWealthTotal;
    }
}