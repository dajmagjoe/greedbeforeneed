﻿namespace Messages
{
    public class PlayerHealthUpdateMessage
    {
        public int PlayerId;
        public int HealthDelta;
        public int TotalHealth;
    }
}