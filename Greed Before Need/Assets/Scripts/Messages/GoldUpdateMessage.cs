﻿namespace Messages
{
    public class GoldUpdateMessage
    {
        public int PlayerId;
        public int GoldDelta;
        public int TotalGold;
    }
}
