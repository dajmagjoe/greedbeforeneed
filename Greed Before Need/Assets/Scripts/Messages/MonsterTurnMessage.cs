﻿using Logic;

namespace Messages
{
    public class MonsterTurnMessage 
    {
        // DW: Naming. Also, do we really need to send the monster?
        public Monster monster;
    }
}
