﻿using System.Collections.Generic;

//This is borrowed class that add some functionality to lists -- most importantly, the ability to shuffle.
// DW: Add link of where you got it. Just in case you need to reference it again
namespace Infrastructure
{
    public static class ListExtensions
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            System.Random rnd = new System.Random();
            for (var i = 0; i < list.Count; i++)
                list.Swap(i, rnd.Next(i, list.Count));
        }

        public static void Swap<T>(this IList<T> list, int i, int j)
        {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    }
}
